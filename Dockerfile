#FROM php:5.6-apache
FROM wordpress:latest

RUN touch /usr/local/etc/php/conf.d/upload-limit.ini \ 
  && echo "upload_max_filesize = 32M" >> /usr/local/etc/php/conf.d/upload-limit.ini \ 
  && echo "post_max_size = 32M" >> /usr/local/etc/php/conf.d/upload-limit.ini

RUN a2enmod rewrite headers expires

# install the PHP extensions we need
RUN apt-get update && apt-get install -y unzip rsync && rm -rf /var/lib/apt/lists/* 

VOLUME /var/www/html

COPY docker-entrypoint.sh /entrypoint.sh

# grr, ENTRYPOINT resets CMD now
ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
